package com.circleci.connector.gitlab.singleorg.client;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.circleci.client.v2.ApiException;
import com.circleci.client.v2.api.DefaultApi;
import com.circleci.client.v2.model.PipelineLight;
import com.circleci.client.v2.model.TriggerPipelineParameters;
import com.circleci.client.v2.model.Workflow.StatusEnum;
import com.circleci.client.v2.model.WorkflowListResponse;
import com.circleci.connector.gitlab.singleorg.model.ImmutablePipeline;
import com.circleci.connector.gitlab.singleorg.model.ImmutableWorkflow;
import com.circleci.connector.gitlab.singleorg.model.Pipeline;
import com.circleci.connector.gitlab.singleorg.model.Workflow.State;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import io.dropwizard.jackson.Jackson;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.ws.rs.ClientErrorException;
import org.junit.jupiter.api.Test;

class CircleCiTest {

  private static final ObjectMapper MAPPER = Jackson.newObjectMapper();
  private static final DefaultApi CIRCLECI_HAPPY;
  private static final DefaultApi CIRCLECI_404;
  private static final DefaultApi CIRCLECI_404_JSON;
  private static final DefaultApi CIRCLECI_500;

  private static final UUID PIPELINE_ID = UUID.randomUUID();
  private static final String PIPELINE_REVISION = "foo";
  private static final UUID WORKFLOW_ID_1 = UUID.randomUUID();
  private static final UUID WORKFLOW_ID_2 = UUID.randomUUID();

  private static final ImmutablePipeline PIPELINE_WITHOUT_ID =
      ImmutablePipeline.of(null, 1, PIPELINE_REVISION, "branch");
  private static final ImmutablePipeline PIPELINE_WITH_ID =
      ImmutablePipeline.copyOf(PIPELINE_WITHOUT_ID).withId(PIPELINE_ID);

  private static final ImmutableWorkflow WORKFLOW_1 =
      ImmutableWorkflow.of(WORKFLOW_ID_1, "my-workflow", State.RUNNING);
  private static final ImmutableWorkflow WORKFLOW_2 =
      ImmutableWorkflow.of(WORKFLOW_ID_2, "another-workflow", State.RUNNING);

  private static final PipelineLight PIPELINE_LIGHT;

  private static final WorkflowListResponse CIRCLECI_WORKFLOW_LIST_RESPONSE_PAGE_1;
  private static final WorkflowListResponse CIRCLECI_WORKFLOW_LIST_RESPONSE_PAGE_2;
  private static final com.circleci.client.v2.model.Workflow CIRCLECI_WORKFLOW_1;
  private static final com.circleci.client.v2.model.Workflow CIRCLECI_WORKFLOW_2;

  static {
    CIRCLECI_HAPPY = mock(DefaultApi.class);
    CIRCLECI_404 = mock(DefaultApi.class);
    CIRCLECI_404_JSON = mock(DefaultApi.class);
    CIRCLECI_500 = mock(DefaultApi.class);

    PIPELINE_LIGHT = new PipelineLight();
    PIPELINE_LIGHT.setId(PIPELINE_ID);

    CIRCLECI_WORKFLOW_1 = new com.circleci.client.v2.model.Workflow();
    CIRCLECI_WORKFLOW_1.setId(WORKFLOW_ID_1);
    CIRCLECI_WORKFLOW_1.setName("my-workflow");
    CIRCLECI_WORKFLOW_1.setStatus(StatusEnum.RUNNING);
    CIRCLECI_WORKFLOW_2 = new com.circleci.client.v2.model.Workflow();
    CIRCLECI_WORKFLOW_2.setId(WORKFLOW_ID_2);
    CIRCLECI_WORKFLOW_2.setName("another-workflow");
    CIRCLECI_WORKFLOW_2.setStatus(StatusEnum.RUNNING);
    CIRCLECI_WORKFLOW_LIST_RESPONSE_PAGE_1 = new WorkflowListResponse();
    CIRCLECI_WORKFLOW_LIST_RESPONSE_PAGE_1.setItems(ImmutableList.of(CIRCLECI_WORKFLOW_1));
    CIRCLECI_WORKFLOW_LIST_RESPONSE_PAGE_1.setNextPageToken("2");
    CIRCLECI_WORKFLOW_LIST_RESPONSE_PAGE_2 = new WorkflowListResponse();
    CIRCLECI_WORKFLOW_LIST_RESPONSE_PAGE_2.setItems(ImmutableList.of(CIRCLECI_WORKFLOW_2));
    CIRCLECI_WORKFLOW_LIST_RESPONSE_PAGE_2.setNextPageToken(null);

    try {
      when(CIRCLECI_HAPPY.triggerPipeline(
              anyString(), anyString(), anyString(), any(TriggerPipelineParameters.class)))
          .thenReturn(PIPELINE_LIGHT);
      when(CIRCLECI_HAPPY.listWorkflowsByPipelineId(PIPELINE_ID, null))
          .thenReturn(CIRCLECI_WORKFLOW_LIST_RESPONSE_PAGE_1);
      when(CIRCLECI_HAPPY.listWorkflowsByPipelineId(PIPELINE_ID, "2"))
          .thenReturn(CIRCLECI_WORKFLOW_LIST_RESPONSE_PAGE_2);
      when(CIRCLECI_HAPPY.getWorkflowById(WORKFLOW_ID_1)).thenReturn(CIRCLECI_WORKFLOW_1);

      when(CIRCLECI_404.triggerPipeline(
              anyString(), anyString(), anyString(), any(TriggerPipelineParameters.class)))
          .thenThrow(new ApiException(404, "No such project"));
      when(CIRCLECI_404.getPipelineById(any(UUID.class)))
          .thenThrow(new ApiException(404, "No such pipeline"));
      when(CIRCLECI_404.getWorkflowById(any(UUID.class)))
          .thenThrow(new ApiException(404, "No such workflow"));

      when(CIRCLECI_404_JSON.triggerPipeline(
              anyString(), anyString(), anyString(), any(TriggerPipelineParameters.class)))
          .thenThrow(new ApiException(404, "{\"message\":\"No such project\"}"));
      when(CIRCLECI_404_JSON.getPipelineById(any(UUID.class)))
          .thenThrow(new ApiException(404, "{\"message\":\"No such pipeline\"}"));
      when(CIRCLECI_404_JSON.getWorkflowById(any(UUID.class)))
          .thenThrow(new ApiException(404, "{\"message\":\"No such workflow\"}"));

      when(CIRCLECI_500.triggerPipeline(
              anyString(), anyString(), anyString(), any(TriggerPipelineParameters.class)))
          .thenThrow(new ApiException(500, "CircleCI is broken"));
      when(CIRCLECI_500.getPipelineById(any(UUID.class)))
          .thenThrow(new ApiException(500, "CircleCI is broken"));
      when(CIRCLECI_500.getWorkflowById(any(UUID.class)))
          .thenThrow(new ApiException(500, "CircleCI is broken"));
    } catch (ApiException e) {
      // Ignore when mocking
    }
  }

  @Test
  void allWorkflowsStatesCovered() {
    List<StatusEnum> states = Arrays.asList(StatusEnum.values());
    assertEquals(new HashSet<>(states), CircleCi.CIRCLECI_TO_WORKFLOW_STATE_MAP.keySet());
  }

  @Test
  void refreshPipelineIfCircleCiReturns4xxWeThrow() {
    CircleCi circleCi = new CircleCi(CIRCLECI_404);
    assertThrows(RuntimeException.class, () -> circleCi.refreshPipeline(PIPELINE_WITH_ID));
  }

  @Test
  void refreshPipelineIfCircleCiReturns4xxWithAJsonMessageWeThrow() {
    CircleCi circleCi = new CircleCi(CIRCLECI_404_JSON);
    assertThrows(RuntimeException.class, () -> circleCi.refreshPipeline(PIPELINE_WITH_ID));
  }

  @Test
  void refreshPipelineIfCircleCiReturns500WeThrow() {
    CircleCi circleCi = new CircleCi(CIRCLECI_500);
    assertThrows(RuntimeException.class, () -> circleCi.refreshPipeline(PIPELINE_WITH_ID));
  }

  @Test
  void refreshPipelineSuccess() {
    CircleCi circleCi = new CircleCi(CIRCLECI_HAPPY);
    Pipeline pipeline = circleCi.refreshPipeline(PIPELINE_WITH_ID);
    assertNotNull(pipeline);
    assertEquals(PIPELINE_LIGHT.getId(), pipeline.id());
    assertEquals(Set.of(WORKFLOW_1, WORKFLOW_2), pipeline.workflows());
  }

  @Test
  void refreshWorkflowIfCircleCiReturns4xxWeThrow() {
    CircleCi circleCi = new CircleCi(CIRCLECI_404);
    assertThrows(RuntimeException.class, () -> circleCi.refreshWorkflow(WORKFLOW_1));
  }

  @Test
  void refreshWorkflowIfCircleCiReturns4xxWithAJsonMessageWeThrow() {
    CircleCi circleCi = new CircleCi(CIRCLECI_404_JSON);
    assertThrows(RuntimeException.class, () -> circleCi.refreshWorkflow(WORKFLOW_1));
  }

  @Test
  void refreshWorkflowIfCircleCiReturns500WeThrow() {
    CircleCi circleCi = new CircleCi(CIRCLECI_500);
    assertThrows(RuntimeException.class, () -> circleCi.refreshWorkflow(WORKFLOW_1));
  }

  @Test
  void refreshWorkflowSuccess() {
    CircleCi circleCi = new CircleCi(CIRCLECI_HAPPY);
    com.circleci.connector.gitlab.singleorg.model.Workflow workflow =
        circleCi.refreshWorkflow(WORKFLOW_1);
    assertNotNull(workflow);
    assertEquals(CIRCLECI_WORKFLOW_1.getId(), workflow.id());
  }

  @Test
  void triggerPipelineIfCircleCiReturns4xxWePassItOn() {
    CircleCi circleCi = new CircleCi(CIRCLECI_404);
    assertThrows(
        ClientErrorException.class,
        () -> circleCi.triggerPipeline(PIPELINE_WITHOUT_ID, "", "", "", "", "", ""));
  }

  @Test
  void triggerPipelineIfCircleCiReturns4xxWithAJsonMessageWePassItOn() {
    CircleCi circleCi = new CircleCi(CIRCLECI_404_JSON);
    assertThrows(
        ClientErrorException.class,
        () -> circleCi.triggerPipeline(PIPELINE_WITHOUT_ID, "", "", "", "", "", ""));
  }

  @Test
  void triggerPipelineIfCircleCiReturns500WeThrowRuntimeException() {
    CircleCi circleCi = new CircleCi(CIRCLECI_500);
    assertThrows(
        RuntimeException.class,
        () -> circleCi.triggerPipeline(PIPELINE_WITHOUT_ID, "", "", "", "", "", ""));
  }

  @Test
  void triggerPipelineIfPipelineAlreadyTriggeredError() {
    CircleCi circleCi = new CircleCi(CIRCLECI_HAPPY);

    assertThrows(
        RuntimeException.class,
        () -> circleCi.triggerPipeline(PIPELINE_WITH_ID, "", "", "", "", "", ""));
  }

  @Test
  void triggerPipelineIfCircleCiReturnsPipelineSuccess() {
    CircleCi circleCi = new CircleCi(CIRCLECI_HAPPY);
    Pipeline pipeline = circleCi.triggerPipeline(PIPELINE_WITHOUT_ID, "", "", "", "", "", "");
    assertNotNull(pipeline);
    assertEquals(PIPELINE_LIGHT.getId(), pipeline.id());
  }
}
